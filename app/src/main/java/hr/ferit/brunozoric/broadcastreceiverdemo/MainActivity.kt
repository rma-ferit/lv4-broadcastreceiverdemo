package hr.ferit.brunozoric.broadcastreceiverdemo

import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val receiver = CustomReceiver()
    val intentFilter = IntentFilter(CustomReceiver.ACTION_CUSTOM)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        broadcastSendAction.setOnClickListener{ broadacastCustomIntent()}
    }

    private fun broadacastCustomIntent() {
        val message = messageInput.text.toString()
        val intent = Intent()
        intent.apply{
            putExtra(CustomReceiver.KEY_MESSAGE, message)
            action = CustomReceiver.ACTION_CUSTOM
        }
        sendBroadcast(intent)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(receiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }
}
