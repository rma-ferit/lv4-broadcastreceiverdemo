package hr.ferit.brunozoric.broadcastreceiverdemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast



class CustomReceiver: BroadcastReceiver() {

    companion object {
        const val KEY_MESSAGE = "message"
        const val ACTION_CUSTOM: String = "hr.ferit.broadcast.CUSTOM_INTENT"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {
            val s = intent?.getStringExtra(KEY_MESSAGE) ?: it.getString(R.string.noMessage)
            Toast.makeText(it, s, Toast.LENGTH_SHORT).show()
        }
    }
}